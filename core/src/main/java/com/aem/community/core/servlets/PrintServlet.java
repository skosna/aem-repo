package com.aem.community.core.servlets;

import org.apache.felix.scr.annotations.Component;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(name = " Test Component" , metatype=true, immediate = true)
public class PrintServlet extends SlingAllMethodsServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9157017533570740352L;
	private static final Logger LOG = LoggerFactory.getLogger(PrintServlet.class);;
	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
		
		LOG.info("In DO Get Mehod");
    }
	
}